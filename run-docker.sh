#!/usr/bin/env bash
sudo docker run -v $PWD:/volume \
    -w /volume --rm \
    -t ghcr.io/hermitcore/hermit-toolchain:latest $1
